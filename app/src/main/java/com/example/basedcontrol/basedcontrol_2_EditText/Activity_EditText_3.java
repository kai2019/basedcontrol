package com.example.basedcontrol.basedcontrol_2_EditText;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.basedcontrol.R;

public class Activity_EditText_3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_text_3);
        EditText editText3_1 = (EditText) findViewById(R.id.editText3_1);
        Button editText3_1_Button = (Button) findViewById(R.id.editText3_1_Button);
        editText3_1_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SpannableString spanStr = new SpannableString("imge");
                Drawable drawable = Activity_EditText_3.this.getResources().getDrawable(R.drawable.ic_launcher_background);
                drawable.setBounds(0,0,drawable.getIntrinsicWidth(),drawable.getIntrinsicHeight());
                ImageSpan span = new ImageSpan(drawable,ImageSpan.ALIGN_BASELINE);
                spanStr.setSpan(span,0,4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                int cursor = editText3_1.getSelectionStart();
                editText3_1.getText().insert(cursor, spanStr);
            }
        });
    }
}