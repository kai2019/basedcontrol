package com.example.basedcontrol.basedcontrol_2_EditText;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.basedcontrol.R;

public class Activity_EditText extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_text);
        Button editText2 = (Button) findViewById(R.id.editText2);
        editText2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Activity_EditText.this, Activity_EditText_2.class);
                startActivity(intent);
            }
        });
    }
}