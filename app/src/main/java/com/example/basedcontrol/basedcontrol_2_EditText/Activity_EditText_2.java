package com.example.basedcontrol.basedcontrol_2_EditText;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.basedcontrol.R;

public class Activity_EditText_2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_text_2);
        EditText editText2_1 = (EditText) findViewById(R.id.editText2_1);
        editText2_1.requestFocus();//获取了焦点
        //editText2_1.clearFocus(); //清除焦点
        /** 无用
         * InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
         * imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
         */
        editText2_1.setFocusable(true);
        editText2_1.setFocusableInTouchMode(true);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);


        EditText editText2_2 = (EditText) findViewById(R.id.editText2_2);
        String editText2_2_text = editText2_2.getText().toString();
        editText2_2.setSelection(editText2_2_text.length());//到最后

        EditText editText2_3 = (EditText) findViewById(R.id.editText2_3);
        String editText2_3_text = editText2_3.getText().toString();
        editText2_3.setSelection(2,editText2_3_text.length());//到最后



        EditText editText2_4 = (EditText) findViewById(R.id.editText2_4);
        editText2_4.setSelectAllOnFocus(true);//得焦点时选中全部文本！
        EditText editText2_5 = (EditText) findViewById(R.id.editText2_5);
        editText2_5.setCursorVisible(false);//设置光标不显示
        //还可以调用getSelectionStart()和getSelectionEnd获得当前光标的前后位置


        Button editText3_Button = (Button) findViewById(R.id.editText3_Button);
        editText3_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Activity_EditText_2.this, Activity_EditText_3.class);
                startActivity(intent);
            }
        });
    }
}