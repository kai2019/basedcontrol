package com.example.basedcontrol.basedcontrol_1_textView;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.basedcontrol.MainActivity;
import com.example.basedcontrol.R;

public class Activity_textView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_view);
        Button TextView_attributes = (Button) findViewById(R.id.TextView_attributes);
        TextView_attributes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Activity_textView.this, TextView_1_CommonlyAttributes.class);
                startActivity(intent);
            }
        });
        Button TextView_function1 = (Button) findViewById(R.id.TextView_function1);
        TextView_function1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Activity_textView.this, TextView_2_CommonFunction_1.class);
                startActivity(intent);
            }
        });
        Button TextView_function2 = (Button) findViewById(R.id.TextView_function2);
        TextView_function2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Activity_textView.this, TextView_2_CommonFunction_2.class);
                startActivity(intent);
            }
        });
    }
}