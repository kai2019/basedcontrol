package com.example.basedcontrol;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.basedcontrol.basedcontrol_1_textView.Activity_textView;
import com.example.basedcontrol.basedcontrol_2_EditText.Activity_EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button textView_button = (Button) findViewById(R.id.textView_button);
        textView_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Activity_textView.class);
                startActivity(intent);
            }
        });
        Button editText = (Button) findViewById(R.id.editText);
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Activity_EditText.class);
                startActivity(intent);
            }
        });
    }
}